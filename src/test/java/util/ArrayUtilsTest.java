package util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class ArrayUtilsTest {

    @Test
    public void arrayContainsValueTest() {
        boolean contains = ArrayUtils.contains(new int[]{1, 2, 3}, 2);

        assumeTrue(contains);
    }

    @Test
    public void addValueToArrayTest() {
        int[] ints = ArrayUtils.addToArray(new int[]{1, 2, 3}, 4);
        List<Integer> you  = Arrays.stream(ints).boxed().collect( Collectors.toList() );
        assertThat(you, contains(1,2,3,4));
    }

}